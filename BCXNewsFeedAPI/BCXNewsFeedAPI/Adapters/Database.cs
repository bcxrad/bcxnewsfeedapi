﻿using System;
using System.Data.SqlClient;

namespace BCXNewsFeedAPI.Adapters
{
    public class Database
    {
        String defaultQuery = "INSERT INTO NEWS_FEED (news_image_url, news_heading, news_body, news_date) " +
      "Values ('string', 1)";
        String connString = "Server=tcp:highvelocitydb.database.windows.net,1433;" +
            "Initial Catalog=employeeapp;Persist Security Info=False;" +
            "User ID=barryj;Password=Password@123;" +
            "MultipleActiveResultSets=False;Encrypt=True;" +
            "TrustServerCertificate=False;Connection Timeout=30;";/*"user id=barryj;" +
            "password=Password@123;server=tcp:highvelocitydb.database.windows.net,1433;" +
                                       "Trusted_Connection=yes;" +
                                       "database=employeeapp; " +
            "connection timeout=30";*/
        SqlConnection conn;

        public Database()
        {
        }

        public void Close()
        {
            try
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                conn = null;
            }
        }

        public object GetConnection()
        {
            return conn;
        }

        public void Open()
        {
            try
            {
                conn = new SqlConnection(connString);
                conn.Open();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public object RunQuery(string query)
        {
            return new SqlCommand(query, conn);
        }
    }
}
