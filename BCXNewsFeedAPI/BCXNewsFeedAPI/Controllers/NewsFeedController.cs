﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using BCXNewsFeedAPI.Adapters;
using BCXNewsFeedAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Cors;
using BCXNewsFeedAPI.Cors;

namespace BCXNewsFeedAPI.Controllers
{
    [Route("api/[controller]")]
    public class NewsFeedController : Controller
    {
        private const String BLOB_STORAGE_URL = "https://highvelocitystorage.blob.core.windows.net/employee-app/";
        private const String SELECT_NEWS_ENTRIES = "SELECT * FROM NEWS_FEED ORDER BY NEWS_DATE DESC";
        private const String INSERT_NEWS_ENTRY = "INSERT INTO NEWS_FEED (NEWS_IMAGE_URL, NEWS_HEADING, NEWS_BODY, NEWS_DATE) " +
                                                    "VALUES (@imageUrl,@heading,@body,@date)";
        private const int NEWS_LIMIT = 5;

        // GET api/newsfeed
        [HttpGet]
        public NewsFeed Get()
        {
            dynamic news = new JObject();
            NewsFeed newsFeed = new NewsFeed();
            List<NewsEntry> newsEntries = new List<NewsEntry>();
            Database db = new Database();
            try
            {
                db.Open();
                SqlCommand sqlComm = (SqlCommand)db.RunQuery(SELECT_NEWS_ENTRIES);
                SqlDataReader myReader = null;
                myReader = sqlComm.ExecuteReader();
                int count = 1;
                while (myReader.Read() && count <= NEWS_LIMIT)
                {
                    NewsEntry newsEntry = new NewsEntry();
                    newsEntry.imageUrl = BLOB_STORAGE_URL + myReader["news_image_url"].ToString();
                    newsEntry.heading = myReader["news_heading"].ToString();
                    newsEntry.body = myReader["news_body"].ToString();
                    newsEntry.date = myReader["news_date"].ToString();
                    newsEntries.Add(newsEntry);
                    count++;
                }
                newsFeed.transactionId = "oie928hr4oubf4";
                newsFeed.newsEntries = newsEntries;
                news.newsFeed = JsonConvert.SerializeObject(newsFeed);
            }
            catch (Exception e)
            {
                news = e.ToString();
            }
            finally
            {
                db.Close();
            }
            return newsFeed;
        }

        // POST api/newsfeed
        [HttpPost]
        public void Post([FromBody]JObject news)
        {
            Database db = new Database();
            try
            {
                db.Open();
                SqlCommand sqlComm = (SqlCommand)db.RunQuery(INSERT_NEWS_ENTRY);
                sqlComm.Parameters.AddWithValue("@imageUrl",(string)news.GetValue("imageUrl"));
                sqlComm.Parameters.AddWithValue("@heading",(string) news.GetValue("heading"));
                sqlComm.Parameters.AddWithValue("@body",(string) news.GetValue("body"));
                sqlComm.Parameters.AddWithValue("@date", DateTime.Now);
                sqlComm.ExecuteNonQuery();
            } catch (Exception e) {
                //return e.ToString();
            } finally {
                db.Close();
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "";
        }
    }
}
