﻿using System;
namespace BCXNewsFeedAPI.Models
{
    public class NewsFeedPost
    {
        public NewsFeedPost()
        {
        }

        public String transactionId { get; set; }
        public String heading { get; set; }
        public String body { get; set; }
    }
}
